# OpenML dataset: diggle_table_a1

https://www.openml.org/d/693

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

DATA-SETS FROM DIGGLE, P.J. (1990). TIME SERIES : A BIOSTATISTICAL
INTRODUCTION. Oxford University Press.

Table: Table A1 Lutenizing hormone


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/693) of an [OpenML dataset](https://www.openml.org/d/693). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/693/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/693/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/693/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

